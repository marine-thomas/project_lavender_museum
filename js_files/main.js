/*
    APPELER DES COMPOSANTS HTML DANS D'AUTRES FICHIERS
*/ 

function callComponent(component, selector){
    fetch(component)
    .then(response => {
        return response.text();
    })
    .then((data) => {
        document.querySelector(selector).innerHTML = data
    });
}

callComponent("./components/header.html", 'header');
callComponent("./components/footer.html", 'footer');
callComponent("./components/navbar.html", 'nav.homepage');
callComponent("informations.html", 'nav.infoPage');

/*
    VERSION MOBILE AFFICHER LE MENU
*/ 

//Permettre au bouton du header d'afficher la page menu
function showMenu() {
    const navbar = document.querySelector('nav');
    navbar.style.display = 'block';
}

function hideMenu() {
    const navbar = document.querySelector('nav');
    navbar.style.display = 'none';
}

function showInformations() {
    const informations = document.querySelector('nav.infoPage');
    informations.style.display = 'block';
}

function hideInformations() {
    const informations = document.querySelector('nav.infoPage');
    informations.style.display = 'none';
}


/*
    GESTION DU FORMULAIRE DE CONTACT
*/ 

/** Détails du contenu de mon expression régulière
 * / ... / -> les barres me permettent de déclarer mon expression régulière
 * ^ -> la vérification se fait en début de chaîne
 * [] -> on découpe la regex en plusieurs morceaux, tous les caractères indiqués à l'intérieur ne seront pas autorisés
 * \. -> correspond au point littéral
*/
let verifyMail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; // Ici je créé une expression régulière pour vérifier les caractères non autorisés dans mon champs email 

//On récupère ici les éléments dont on aura besoin 
const form = document.querySelector('.form_contact');
const buttonSubmit = document.getElementById('button_validation');
const modal = document.querySelector('.modal'); 
const buttonCloseModal = document.getElementById('modal_close');

function submitForm(event) {
    event.preventDefault();
    
    //Si tous les champs du formulaire sont remplis ET si le champs email est au bon format
    if((form.email.value !== "" && form.subject.value !== "" && form.message.value !== "") && verifyMail.test(form.email.value)){
        modal.style.display = 'block';//Afficher une popup
    } else {
        //Sinon afficher des messages d'erreurs
        if(form.email.value === ""){ //Si le champs email est vide
            form.email.value = 'Veuillez saisir une adresse';
            styleInput('email');
        } else if(!verifyMail.test(form.email.value)){ //Sinon si l'email
            form.email.value = 'Il y a une erreur dans votre adresse mail, veuillez la ressaisir';
            styleInput('email');
        }

        if(form.subject.value === ""){
            form.subject.value = 'Veuillez renseigner un sujet';
            styleInput('subject');
        }

        if(form.message.value === ""){
            form.message.value = 'Vous n\'avez pas indiqué de message';
            styleInput('message');
        }
    }
}

function styleInput(inputValue){
    form[inputValue].style.border = '2px solid red';
    form[inputValue].style.color = 'red';
    form[inputValue].style.fontWeight = 'bold';
}

function closeModal(){
    modal.style.display = 'none'; //Retirer la modale
    form.email.value = ""; //Vider ce champs
    form.subject.value = ""; //Vider ce champs
    form.message.value = ""; //Vider ce champs
}

if(buttonSubmit !== null){
    buttonSubmit.addEventListener('click', submitForm); //La validation du formulaire se fait au clic sur le bouton submit
    buttonCloseModal.addEventListener('click', closeModal); //La popup se fermera au clic sur le bouton close
}

/*
    GESTION DES COMMENTAIRES
*/ 

//Récupérer les éléments dont nous avons besoin
const formComments = document.querySelector('.form_comments'); //Le formulaire
const buttonComment = document.getElementById('button_comment'); //Le bouton de validation du form
const visitorCommentsBox = document.querySelector('.visitors_comments'); //L'encart qui contient les commentaires à afficher seulement s'il y a des commentaires
const visitorCommentContent = document.getElementById('visitors_comments_content'); //La section qui contiendra la liste des commentaires

//Créer un ul
const listComments = document.createElement('ul');
if(visitorCommentContent !== null){
    visitorCommentContent.append(listComments);
}
showComments(); //Appeler ma fonction une première fois au chargement de la page pour vérifier si des commentaires existent ou non

//faire une function pour le bouton submit
function submitComment(event){
    event.preventDefault();
    const textAreaBox = formComments.comments; //Zone de texte
    const textAreaContent = textAreaBox.value; //Récupérer le contenu de la zone de texte
    const addItems = document.createElement('li'); //Créer dans le DOM un li
    addItems.classList.add('list_comments_item'); //Ce li sera rajouté 

    //Ajouter Le contenu en tant que li
    listComments.appendChild(addItems);
    addItems.append(textAreaContent);

    //Remettre à 0 le formulaire
    textAreaBox.value = "";
    showComments(); //Revérifier si des commentaires existent, normalement oui, dans ce cas montrer l'espace commentaires
}

//Affichage des commentaires des visiteurs
function showComments(){
    if(listComments.childElementCount > 0) { //Retourner le nombre d'éléments enfants de mon élément listComments
        if(visitorCommentsBox !== null){
            visitorCommentsBox.style.display = 'block';
        }
    } else {
        if(visitorCommentsBox !== null){
            visitorCommentsBox.style.display = 'none';
        }
    }
}
//Ecouter le bouton submit comment
if(buttonComment !== null){
    buttonComment.addEventListener('click', submitComment);
}


/*
    CAROUSEL IMAGES
*/ 

//Récupérer le carousel
const slider = document.querySelector('.slideshow_box_images');

//récupérer chaque image du carousel
const imageSlide = document.querySelectorAll('.slideshow_image'); 
let imageWidth = imageSlide[0].offsetWidth; //Permet de mesurer la taille de l'élément
let currentSlide = 0;

//Créer une fonction qui modifiera la taille du carousel 
function slideUpdate() {
    slider.style.transform = `translateX(-${currentSlide * imageWidth}px)`;
}

//Récupérer les boutons prev et next
const prev = document.getElementById('button_prev');
const next = document.getElementById('button_next');

//Créer les fonctions qui vont permettre aux boutons de faire défiler les images
function buttonNext(){
    currentSlide = (currentSlide + 1) % imageSlide.length;
    slideUpdate();
}
function buttonPrev(){
    currentSlide = (currentSlide - 1 + imageSlide.length) % imageSlide.length;
    slideUpdate();
}

next.addEventListener('click', buttonNext);
prev.addEventListener('click', buttonPrev);